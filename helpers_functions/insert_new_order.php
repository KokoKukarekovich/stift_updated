<?php

function insert_new_order () {
  include ($_SERVER['DOCUMENT_ROOT']."/includes/db_conn.php");

  try {
    $stmt = $dbh->prepare("SELECT custo_id FROM stift.auth_tokens_custo WHERE cookie_token =:cookie_token");
    $stmt->bindParam(':cookie_token', $_COOKIE['rememberme']);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e) {
    $error = 'cannot select customer_id '.$e->getMessage();
    include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }

  session_start();
  //uploaded files proceeding
  if ($_FILES['drawing']['error'] > 0) {
    $error = 'ERROR: ';
    switch ($_FILES['drawing']['error']) {
      case 1: $error_file = 'File exceeded upload_max_filesize'; break;
      case 2: $error_file = 'File exceeded MAX_FILE_SIZE'; break;
      case 3: $error_file = 'File only partially uploaded'; break;
      case 4: $error_file = 'No file uploaded'; break;
      case 6: $error_file = 'Temporary folder does not exist'; break;
      case 7: $error_file = 'PHP extension stopped file uploading'; break;
    }
    include ($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }

  if ($_FILES['drawing']['size'] > 2000000) {
    $error = "File size exceeded upload max size";
    include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }

  //MIME types check
  $mime_allowed = array ('jpg' => 'image/jpeg',
                        'png' => 'image/png',
                        'tiff' => 'image/tiff',
                        'pdf' => 'application/pdf',
                        'zip' => 'application/zip'
                        );
  $mime_of_file = mime_content_type($_FILES['drawing']['tmp_name']);

  if (!array_search($mime_of_file, $mime_allowed)) {
    $error = "Please, change file extension to jpg, png, tiff, pdf or zip";
    include ($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }

    $ext = array_search($mime_of_file, $mime_allowed);//Do I need this???

  // Further info from php.net --> you should name fales uniquely
  //DO not use $_FILES['drawing']['name'] without any validation
  if (!is_uploaded_file($_FILES['drawing']['tmp_name'])) {
    $error = "Failed to move uploaded file";
    include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }

  $upload_dir = $_SERVER['DOCUMENT_ROOT']."/drawings/".$result['custo_id']."_".$_FILES['drawing']['name'];

  $file_chname = $result['custo_id']."_".$_FILES['drawing']['name'];

  if (!move_uploaded_file($_FILES['drawing']['tmp_name'], $upload_dir)) {
    $error = "Failed to move uploaded file";
    include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
    }

  try {
    $stmt = $dbh->prepare("INSERT INTO stift.orders (customer_id, detail_name, detail_quantity, detail_length, detail_width, detail_height,
                          detail_mass, detail_treatment, work_type, work_piece, publicasion_date, need_to_be_date, drawing, status_id) VALUES (:customer_id, :detail_name, :detail_quantity,
                          :detail_length, :detail_width, :detail_height, :detail_mass, :detail_treatment, :work_type, :work_piece, NOW(), :need_to_be_date,
                          :drawing, 1)");
    $stmt->bindParam(':customer_id', $result['custo_id']);
    $stmt->bindParam(':detail_name', $_POST['detail_name']);
    $stmt->bindParam(':detail_quantity', $_POST['detail_quantity']);
    $stmt->bindParam(':detail_length', $_POST['detail_length']);
    $stmt->bindParam(':detail_width', $_POST['detail_width']);
    $stmt->bindParam(':detail_height', $_POST['detail_height']);
    $stmt->bindParam(':detail_mass', $_POST['detail_mass']);
    $stmt->bindParam(':detail_treatment', $_POST['detail_treatment']);
    $stmt->bindParam(':work_type', $_POST['work_type']);
    $stmt->bindParam(':work_piece', $_POST['work_piece']);
    $stmt->bindParam(':need_to_be_date', $_POST['need_to_be_date']);
    $stmt->bindParam(':drawing', $file_chname);

    $stmt->execute();
  }
  catch(PDOException $e) {
    $error = "cannot INSERT new order in DB ".$e->getMessage();
    include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }
  header('Location:.');
  exit();
//include ($_SERVER['DOCUMENT_ROOT']."/views/customer_personal_page/index.php");
//exit();
  }

 ?>
