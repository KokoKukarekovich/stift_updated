<?php
function login_me ($role) {
  include ($_SERVER['DOCUMENT_ROOT']."/includes/db_conn.php");
  $role_id = $role."mer_id";
  $table = "stift.".$role."mers";
  try{
		$stmt = $dbh->prepare ("SELECT phone, password, $role_id, company_name, name_last, name_first FROM $table WHERE phone = :phone");
		$stmt->bindParam(':phone', $_POST['phone']);
		$stmt->execute();
		$result = $stmt->fetch(PDO::FETCH_ASSOC);
	}
	catch(PDOException $e) {
		$error = "cannot retrieve ". $role."mer phone". $e->getMessage();
		include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
		exit();
	}

  if (empty($result)) {
    $error_log = "Проверьте, пожалуйста, правильность введенного Логина";
    include ($_SERVER['DOCUMENT_ROOT']."/views/main_page/index.php");
    exit();
  }

  if (password_verify($_POST['password'], $result['password'])) {
    $table = "stift.auth_tokens_".$role;
    $role_ids = $role."_id";
    //retrieving cookie_token from db
    try {
      $stmt = $dbh->prepare ("SELECT cookie_token FROM $table WHERE $role_ids = :role_id");
      $stmt->bindParam(':role_id', $result[$role_id]);
      $stmt->execute();
      $result_cookie = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
      $error = "cannot retrieve ". $role."mer cookie_token". $e->getMessage();
      include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
      exit();
    }
    setcookie("rememberme", $result_cookie['cookie_token'], strtotime('+365 days'), '/', "", false, true);
    setcookie("role", $role, strtotime('+365 days'), '/', "", false, true);
    session_start();
    if (!isset($_SESSION['user_name']) && !isset($_SESSION['user_lastname']) && !isset($_SESSION['company_name'])) {
      $_SESSION['user_name'] = $result['name_first'];
      $_SESSION['user_lastname'] = $result['name_last'];
      $_SESSION['company_name'] = $result['company_name'];
      $_SESSION['user_id'] = $result[$role_id];
    }
    header('Location:.');
    exit();
  }
  $error_psw = "Проверьте, пожалуйста, правильность введенного Пароля";
  include ($_SERVER['DOCUMENT_ROOT']."/views/main_page/index.php");
  exit();
}

?>
