<?php

function select_prdt (){

if ($_COOKIE['role'] == 'custo') {

  include ($_SERVER['DOCUMENT_ROOT']."/includes/db_conn.php");
  session_start();
  try {
    $stmt = $dbh->prepare ("SELECT orders.detail_name AS наименование, orders.detail_quantity, orders.publicasion_date,
      orders.need_to_be_date, status.status_definition, (SELECT COUNT(*) FROM stift.propositions WHERE propositions.order_id = orders.order_id)
      AS предложения, orders.order_id AS подробнее FROM stift.orders, stift.status
      WHERE orders.status_id = status.status_id AND orders.customer_id = :customer_id ORDER BY orders.publicasion_date" );
      $stmt ->bindParam(':customer_id', $_SESSION['user_id']);
      $stmt->execute();
      $result = $stmt->fetchAll(PDO::FETCH_ASSOC ); //???? returns only one first array, because it was Fetch --> not FetchAll
    }

  catch(PDOException $e) {
    $error =  "Cannot retrive personal data from DB ". $e->getMessage();
    include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
    exit();
  }

  if(empty($result)) {
    $response = "У вас нет размещенных Заказов";
    include ($_SERVER['DOCUMENT_ROOT']."/views/customer_personal_page/index.php");
    exit();
  }
////////////////////////////////////////////////
/////returns detalization of selected order/////
////////////////////////////////////////////////
  if (isset($_GET['ord']))
  { /////*select order's details*/////
    try
    {
      $stmt = $dbh->prepare("SELECT orders.detail_name, orders.detail_quantity, orders.detail_length, orders.detail_width,
        orders.detail_height, orders.detail_mass, orders.detail_treatment, orders.work_type, orders.work_piece, orders.publicasion_date,
        orders.need_to_be_date, status.status_definition FROM stift.orders, stift.status
        WHERE orders.status_id = status.status_id AND orders.order_id = :order_id AND orders.customer_id = :customer_id");
        $stmt->bindParam(':order_id', $_GET['ord']);
        $stmt->bindParam(':customer_id', $_SESSION['user_id']);
        $stmt->execute();
        $order_set = $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    catch (PDOException $e)
    {
      $error = "Cannon select defined order from DB".$e->getMessage();
      include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
      exit();
    }
    /////////////////////////////////////////////////////
    /////*select all propositions for defined order*/////
    /////////////////////////////////////////////////////
    try
    {

      $stmt = $dbh->prepare("SELECT performers.company_name, performers.name_first, performers.name_last, performers.phone,
       propositions.delivery_date_proposition, propositions.pricd_proposition, propositions.proposition_id FROM stift.performers, stift.propositions
       WHERE performers.performer_id = propositions.performer_id AND propositions.order_id = :order_id ORDER BY propositions.pricd_proposition" );
       $stmt->bindParam(":order_id", $_GET['ord']);
       $stmt->execute();
       $prp_set = $stmt->FetchAll(PDO::FETCH_ASSOC);

    }

    catch (PDOException $e)
    {
      $error = "Cannon select propositions from DB".$e->getMessage();
      include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
      exit();
    }
  }

  include ($_SERVER['DOCUMENT_ROOT']."/views/customer_personal_page/index.php");
  exit();

  }
  
///////////////////////////////////
/////PERFORMER'S PERSONAL PAGE/////
///////////////////////////////////
if($_COOKIE['role'] == 'perfor') {
  include ($_SERVER['DOCUMENT_ROOT']."/views/performer_personal_page/index.php");
  exit();
}

}
 ?>
