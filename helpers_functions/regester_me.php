<?php

require ($_SERVER['DOCUMENT_ROOT']."/helpers_functions/trimm_it.php"); //include class Trim

function regester_me(){
  $psw = password_hash($_POST['password'], PASSWORD_DEFAULT);
  include($_SERVER['DOCUMENT_ROOT']."/includes/db_conn.php");
  $table = "stift.".$_POST['role'];

try{
  $dbh->exec('SET NAMES "utf8"');
  $stmt = $dbh->prepare("INSERT INTO $table (company_name, region, city, address, postcode, name_last, name_first, phone, email, website, password)
                        VALUES (:company_name, :region, :city, :address, :postcode, :name_last, :name_first, :phone, :email, :website, :password) ");
  $stmt->bindParam(':company_name', $_POST['company_name']);
  $stmt->bindParam(':region', $_POST['region']);
  $stmt->bindParam(':city', $_POST['city']);
  $stmt->bindParam(':address', $_POST['address']);
  $stmt->bindParam(':postcode', $_POST['postcode']);
  $stmt->bindParam(':name_last', $_POST['name_last']);
  $stmt->bindParam(':name_first', $_POST['name_first']);
  $stmt->bindParam(':phone', $_POST['phone']);
  $stmt->bindParam(':email', $_POST['email']);
  $stmt->bindParam(':website', $_POST['website']);
  $stmt->bindParam(':password', $psw);
  $stmt->execute();
}
catch(PDOException $e) {
  $error = "cannot insert into DB". $e->getMessage();
  include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
  exit();
}
//Generate random token for cookie "rememberme"
require_once ($_SERVER['DOCUMENT_ROOT']."/includes/lib/random.php");

try {
  $value = bin2hex(random_bytes(16));
}

catch(Exception $e) {
  echo "Cannot generate cookie_token ". $e->getMessage();
  include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
  exit();
}

catch(TypeError $e) {
  echo "Cannot generate cookie_token ". $e->getMessage();
  include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
  exit();
}

catch(Error $e) {
  echo "Cannot generate cookie_token ". $e->getMessage();
  include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
  exit();
}
//end of random token generation

setcookie("rememberme", $value, strtotime('+365 days'),  '/', "", false, true); // Expiry time 1 year

$role = new Trim($_POST['role'], 4);
$role = $role->trm();

$table = "stift.auth_tokens_".$role;
$role_id = $role."_id";
setcookie("role", $role, strtotime('+365 days'), '/', "", false, true);
//Putting cookie token in the db
try {
  $stmt = $dbh->prepare("INSERT INTO $table  ($role_id, cookie_token) VALUES (LAST_INSERT_ID(), :cookie_token)");
  $stmt->bindParam(':cookie_token', $value);
  $stmt->execute();
}

catch(PDOException $e) {
  $error =  "Cannot insert cookie_token into DB ". $e->getMessage();
  include ($_SERVER['DOCUMENT_ROOT']. "/views/error_page/index.php");
  exit();
}
//attempt to set $_SESSION['user_id']
try {
  $stmt = $dbh->prepare ("SELECT $role_id FROM $table WHERE cookie_token = :cookie_token");
  $stmt->bindParam(':cookie_token', $value);
  $stmt->execute();
  $result = $stmt->fetch(PDO::FETCH_ASSOC);
}
catch(PDOException $e){
  $error = 'cannot select cookie token from table '.$e->getMessage();
  include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
  exit();
}

session_start();
if (!isset($_SESSION['user_name']) && !isset($_SESSION['user_lastname']) && !isset($_SESSION['company_name'])) {
  $_SESSION['user_name'] = $_POST['name_first'];
  $_SESSION['user_lastname'] = $_POST['name_last'];
  $_SESSION['company_name'] = $_POST['company_name'];
  $_SESSION['user_id'] = $result[$role_id];

}
//End of putting token in db
header('Location:.');
exit();
}

 ?>
