<?php
function isset_cookie($role, $value) {
  include ($_SERVER['DOCUMENT_ROOT']."/includes/db_conn.php");
  $table = "stift.auth_tokens_".$role;
  $role_id = $role."_id";
  //searching cookie token in auth_tokens_$role table
  try {
    $stmt = $dbh->prepare ("SELECT $role_id FROM $table WHERE cookie_token = :cookie_token");
    $stmt->bindParam(':cookie_token', $value);
    $stmt->execute();
    $result = $stmt->fetch(PDO::FETCH_ASSOC);
  }
  catch(PDOException $e){
    $error = 'cannot select cookie token from table '.$e->getMessage();
    include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
    exit();
  }
  //check wether returned result is not empty
  if (!empty($result)) {
    $table = "stift.".$role."mers";
    $role_ids = $role."mer_id";
    try {
      $stmt = $dbh->prepare ("SELECT name_first, name_last, company_name FROM $table WHERE $role_ids = :performer_id");
      $stmt->bindParam(':performer_id', $result[$role_id]);
      $stmt->execute();
      $result = $stmt->fetch(PDO::FETCH_ASSOC);
    }
    catch(PDOException $e) {
      $error = 'cannot select user info from table'. $table." ".$e->getMessage();
      include($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
      exit();
    }

    session_start();
    if (!isset($_SESSION['user_name']) && !isset($_SESSION['user_lastname']) && !isset($_SESSION['company_name'])) {
      $_SESSION['user_name'] = $result['name_first'];
      $_SESSION['user_lastname'] = $result['name_last'];
      $_SESSION['company_name'] = $result['company_name'];
      $_SESSION['user_id'] = $result[$role_id];
    }

    header('Location:.');
    exit();
  }

include ($_SERVER['DOCUMENT_ROOT']."/");
exit();
}
 ?>
