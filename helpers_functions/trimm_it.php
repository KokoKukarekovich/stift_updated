<?php

class Trim {

  public $text;

  public $qnt_char;

  public function __construct($text, $qnt_char)
  {
  $this->text = $text;
  $this->qnt_char = $qnt_char;
  }

  public function trm()
  {
    $this->text = strrev($this->text);

    $this->text = str_split($this->text);

    $this->text = array_slice($this->text, $this->qnt_char);

    $this->text = implode($this->text);

    $this->text = strrev($this->text);

    return $this->text;
  }

}

?>
