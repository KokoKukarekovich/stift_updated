<?php

function render($template, $data = array()) {

	$path = $_SERVER['DOCUMENT_ROOT']."/includes/".$template. '.php';

	if (file_exists($path)) {
		extract($data);
		require($path);
	}

}
?>
