<?php
function logout_me () {

  setcookie("rememberme", FALSE, strtotime('+365 days'),  '/', "", false, true );
	unset($_COOKIE['rememberme']);

  setcookie ("role", FALSE, strtotime('+365 days'), '/', "", false, true );
  unset($_COOKIE['role']);

  session_start();
  session_unset();
  setcookie("PHPSESSID",FALSE);
  unset($_COOKIE['PHPSESSID']);


	include ($_SERVER['DOCUMENT_ROOT']."/views/main_page/index.php");
	exit();
}
 ?>
