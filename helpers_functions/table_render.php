<?php /**
      *function TABLE_RENDER proceed result of DB query
      *array $query contains resulting array of query.
      *int $num defines qauntity of columns in the tail of a table with hyperlinks
      *
      *
      */?>

<?php function table_render($query, $num) {?>

<?php if (!empty($query)): ?>

	<!--TABLE ALL ORDERS-->
<?php $folder_path = ltrim($_SERVER['DOCUMENT_ROOT'], "D:/lampp/htdocs");?>

	<table>
		<tr>
			<?php foreach (array_keys($query[0]) as $column_name): ?>
				<th><strong><?php echo(htmlspecialchars($column_name)); ?></strong></th>
			<?php endforeach; ?>
		</tr>

	<?php
	for ($i=0; $i < count($query); $i++) {

	echo ("<tr>");

	$values = array_values(current($query));

	for ($k=0; $k < count($values); $k++)
	{

    echo ("<td>". htmlspecialchars($values[$k])."</td>");

    if(!empty($num))

    {

      $keys = array_keys($num);

      $value = array_values($num);

		    for ($f = 0; $f < count($num); $f++)

        {

          if ($k == count($values) - (count($num) + $f))

		        {
			         echo ("<td><a href=".$folder_path."/?".$keys[$f]."=".$values[$k].">".$value[$f]."</a></td>");
		        }

        }

      }

    }
	echo ("</tr>");
	next($query);

	}
	?>

	</table>
<?php else: ?>
  <p><?php echo $response; ?></p>
<?php endif; ?>
  <?php } ?>
	<!--TABLE-->
