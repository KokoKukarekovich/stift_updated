<?php
	if (isset ($_COOKIE['rememberme'])) {
	session_start();
	}
	 ?>
<!DOCTYPE html>
<html>
<head>
	<meta name="keywords" content="">
	<meta charset="UTF-8">
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale = 1">

	<title>Заказ деталей оборудования</title>
	<link href='https://fonts.googleapis.com/css?family=Orbitron:500' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Play&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
<!--For browser security issues-(it blocks all request to local folders) added line below -->
<?php $folder_path = ltrim($_SERVER['DOCUMENT_ROOT'], "D:/lampp/htdocs");?>

	<link rel="stylesheet" type="text/css" href= "<?php echo $folder_path."/styles/mystyle.css"?>">
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js">
	</script>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js">
	</script>
	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
	<script>
		$(function() {
			$ ("#datepicker"). datepicker();
		});
	</script>
	<script src = "<?php echo ($folder_path."/jscripts/scripts.js"); ?>"></script>

</head>


	<body>

		<!--HEADER-->
		<header class="header">
			<div>
				<img src="<?php echo ($folder_path."/pictures/logo/stiftlogoinwhite.png");?>">
				<h2>сервис для заказа деталей оборудования</h2>
			</div>
		</header>
		<!--/HEADER-->

		<!--NAVIGATION BAR-->
		<nav>
			<div>
				<ul class="bar">
					<li> <a href="<?php echo ($folder_path."/views/main_page/");?>">Главная</a></li>
					<li> <a href="<?php echo ($folder_path."/views/customer_page/");?>">Заказчику</a></li>
					<li> <a href="<?php echo ($folder_path."/views/performer_page/");?>">Исполнителю</a></li>
					<li> <a href="<?php echo ($folder_path."/views/about_project/");?>">О проекте</a></li>
					<li> <a href="<?php echo ($folder_path."/views/our_contacts/");?>">Контакты</a></li>
					<!--user private page snippet-->
					<?php if (isset($_COOKIE['rememberme']) && isset($_COOKIE['PHPSESSID']) && isset($_COOKIE['role'])): ?>
						<li><a href="<?php echo ($folder_path."/")?>">
						<?php echo "Ваша персональная страница, ".htmlspecialchars($_SESSION['user_name'])." ".htmlspecialchars($_SESSION['user_lastname'])  ?></a></li>
						<li><a href="<?php echo ($folder_path."/?logged_out")?>">Выйти</a></li>
					<?php endif; ?>
					<!--user private page snippet-->
				</ul>
			</div>
		</nav>
		<!--/NAVIGATION BAR-->
