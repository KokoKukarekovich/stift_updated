<?php

try{

	$dbh = new PDO('mysql:host = localhost; dbname = stift; charset = utf8', 'stift_user', 'mf343262');
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	$dbh->exec('SET NAMES "utf8"');
	}

catch(PDOException $e){
			$error = 'cannot connect to database'. $e->getMessage();
			include ($_SERVER['DOCUMENT_ROOT']."/views/error_page/index.php");
		}

?>
