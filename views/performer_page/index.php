<?php require_once($_SERVER['DOCUMENT_ROOT']."/helpers_functions/footer_header.php"); ?>

<?php render ('header'); ?>

<div class="onstiftyoucan">



</div>
<hr>

	<div id="form">
<?php $folder_path = ltrim($_SERVER['DOCUMENT_ROOT'], "D:/lammp/htdocs");?>
		<legend><b>Заполните, пожалуйста, поля регистрационной формы</b> </legend><br>
			<form  action="<?php echo ($folder_path."/"); ?>" method = "POST" onsubmit = "return ValidateForm();">

						<label for="company">Компания:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="company_name" placeholder="Пропустите это поле, если Вы физическое лицо">

						<label for="region">Выберите область:</label>
						<span class = "error1"></span>
						<select id="role_choose" name="region">
						<?php
							require ($_SERVER['DOCUMENT_ROOT']."/includes/region_array.php");
							foreach ($regions as $key => $value) {
							echo "<option value=\"".htmlspecialchars($key)."\">".htmlspecialchars($value)."</option>";
							}
						?>
						</select>

						<label for="city">Название населенного пункта:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="city">

						<label for="address">Адрес:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="address">

						<label for="postcode">Почтовый индекс:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="postcode" maxlength = "5">

						<label for="name_last">Фамилия:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="name_last">

						<label for="name_first">Имя:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="name_first">

						<label for="email">Адрес электронной почты:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="email">

						<label for="website">Веб-сайт:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="website">

						<label for="phone">Контактный телефон (будет использоваться как Логин для входа на сайт):</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="phone">

						<label for="password">Пароль:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="password">

						<label>Повторите, пожалуйста, пароль:</label><br>
						<label><span class = "error"></span></label>
						<input type="text">

			<legend><b>Информация о производственных возможностях</b> </legend>

						<label for "facilities">Укажите типы работ, которые Вы выполняете</label><br>
						<label><input type = "checkbox" value = "welding">Сварочные</label><br>
						<label><input type = "checkbox" value = "turn">Токарные</label><br>
						<label><input type = "checkbox" value = "mill">Фрезерные</label><br>
						<label><input type = "checkbox" value = "boring">Расточные</label><br>
						<label><input type = "checkbox" value = "dolb">Долбежные</label><br>
						<label><input type = "checkbox" value = "treatment">Термообработка</label><br>
						<label><input type = "checkbox" value = "flatgrind">Плоскошлифовальные</label><br>
						<label><input type = "checkbox" value = "shaftgrind">Круглошлифовальные</label><br>
						<label><input type = "checkbox" value = "gearmilling">Зубофрезерные</label><br>
						<label><input type = "checkbox" value = "geardolb">Зубодолбежные</label><br>
						<label><input type = "checkbox" value = "gearshapping">Зубошлифовальные</label><br>
						<label><input type = "checkbox" value = "assembly">Сборочные</label><br>
						<label><input type = "checkbox" value = "rolling">Вальцовка</label><br>
						<label><input type = "checkbox" value = "casting">Литейные работы</label><br>
						<label><input type = "checkbox" value = "forging">Ковка, штамповка</label><br>

						<label for="additional_works">Укажите через запятую дополнительные работы, не вошедшие в список:</label><br>
						<label><span class = "error"></span></label>
						<input type="text" name="additional_works">
						<br><br>

						<input type="hidden" name="role" value="performers">
						<input type = "submit" name="regester_me" value="Зарегистрироваться">

			</form>
	</div>
	<br>


<?php render('footer');?>
