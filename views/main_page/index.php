<?php require_once ($_SERVER['DOCUMENT_ROOT']."/helpers_functions/footer_header.php");?>

<?php render ('header'); ?>
		<!--MAIN SECTION-->
		<div class="onstiftyoucan">
			<h2>На Stifte Вы можете разместить Заказ на изготовление</h2>
				<div>
					<h2>запасных частей</h2>
					<p><img src="/pictures/main_page/wrench.png"></p>
				</div>
				<div>
					<h2>деталей</h2>
					<p><img src="/pictures/main_page/part1.png"></p>
				</div>
				<div>
					<h2>узлов</h2>
					<p><img src="/pictures/main_page/unit.png"></p>
				</div>
				<div>
					<h2>сборок</h2>
					<p><img src="/pictures/main_page/assembly.png"></p>
				</div>
		</div>

		<div class="onstiftyoucan">
			<hr>
			<h2>Для кого создан Stift:</h2>
			<p><img id="imgforwhostift" src="/pictures/main_page/forwho.png"> </p>
		</div>

		<div class = "onstiftyoucan">
			<hr>
			<h2>Зарегистрируйтесь:</h2>
			<div id = "form">
				<?php $folder_path = ltrim($_SERVER['DOCUMENT_ROOT'], "D:/lammp/htdocs");?>
				<a href="<?php echo($folder_path."/views/customer_page/")?>">Зарегистрируйтесь как Заказчик</a><br><br><br><br>
				<a href="<?php echo($folder_path."/views/performer_page/")?>">Зарегистрируйтесь как Исолнитель</a>
			</div>
		</div>

		<div class = "onstiftyoucan">
			<hr>
			<h2>Войдите:</h2>
			<div id = "form">

				<form action = "<?php echo ($folder_path."/");?> " method="POST">
					<label for="tel">Логин(телефон):</label>
					<p class = error><?php if(isset($error_log)) {echo $error_log;}?></p>
					<label><span class = "error"></span></label>
					<input type="text" name="phone" placeholder="Введите номер в формате +380ххххххххх"></input>

					<label for="password">Пароль:</label>
					<p class = error><?php if(isset($error_psw)) {echo $error_psw;}?></p>
					<label><span class="error"></span>
					<input type="password" name="password"></input>
					<input type="radio" name="role" value="custo">как Заказчик<br>
					<input type="radio" name="role" value="perfor">как Исполнитель<br>
					<input type="submit" name="login_me" value="войти"></input><br>
				</form>
			</div>
		</div>




		<!--<div class="onstiftyoucan">
			<hr>
			<h2>Как это работает: </h2>
			<p><img id="imghowitworks" src="pictures/index/path.png"> </p>
		</div>-->
		<!--<div class="onstiftyoucan">
			<hr>
			<h2>Стать частью сообщества Stifta:</h2>

			<div id = "form">

			<form action = "" method = "POST">

				<label for="company">Компания:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="company" placeholder="Пропустите это поле, если Вы физическое лицо">

				<label for="region">Выберите область:</label>
				<span class = "error1"></span>
				<select id="role_choose" name="region">
				<?php

					//require ($_SERVER['DOCUMENT_ROOT']."/includes/region_array.php");
					//foreach ($regions as $key => $value) {
						//echo "<option value=\"".htmlspecialchars($key)."\">".htmlspecialchars($value)."</option>";
					//}
				?>
				</select>

				<label for="city">Название населенного пункта:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="city">

				<label for="lname">Фамилия:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="name_last">

				<label for="name">Имя:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="name_first">

				<label for="lname">Отчество:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="name_middle">

				<label for="phone">Контактный телефон:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="phone">

				<label for="mail">Адрес электронной почты:</label><br>
				<label><span class = "error"></span></label>
				<input type="text" name="mail">

				<label for="role_choose">Кто Вы?:</label>
				<span class = "error1"></span>
				<select id="role_choose" name="role_choose">
					<option value="Исполнитель">Исполнитель</option>
					<option value="Заказчик">Заказчик</option>
				</select>

				<input type = "submit" value = "Зарегистрироваться!">

			</form>

			</div>

		</div>-->
		<!--<div class="onstiftyoucan">
			<hr>
			<h2>Стоимость:</h2>
			<p>На период тестирования и отладки все услуги являются бесплатными</p>
			<p><img class="off" src="pictures/index/off.png"></p>
		</div>-->



		<div class="onstiftyoucan">
			<hr>
			<h2>Отзывы о работе Stifta:</h2>
			<p>Нам необходимы Ваши отзывы, Ваше мнение о нас. Каждый новый отзыв - это возможность стать лучше!</p>
			<p><img class="off" src="pictures/main_page/feedback.png"></p>
			<div>
				<p><b>(097) 404 72 68</b></p>
			</div>
			<div>
				<p><b>(050) 101 66 77</b></p>
			</div>
			<div>
				<p><b>(093) 101 66 77</b></p>
			</div>
			<div>
				<p><b>feedback@stift.ua</b></p>
			</div>
		</div>
		<!--/MAIN SECTION-->



<?php render('footer');?>
